---
categories: Hotels And Restaurants
title: Rancho Pacifico boutique hotel -renovation-
featured: true
project_featured_image: https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790177/1.2.Clubhouse_dvgsux.jpg
project_gallery:
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790181/36.PoolsideTerrace_igecc0.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790195/35.PoolsideBar_ay9adn.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790175/34.PoolandTreeMirrorImages_j0muiq.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790178/33.LuxurySuite_th5oqj.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790103/32.LuxurySuitewithOceanView_qufsvt.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790122/31.LuxurySuiteSoakingTubandOceanView_r4zqg8.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790177/30.LuxurySuiteSinks_nzjuf9.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790108/29.LuxurySuiteBedroom_yyznra.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790127/28.Luxurysuitebedandbath_llhoh7.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790091/27.LuxurySuiteSoakingTub_hn3a7o.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790097/27.Luxurysuitebathproducts_ktrfqi.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790172/26.Luxurysuitesappointments_m93zcl.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790187/22infinitypoolundereveningskies_ys560c.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790165/21.Infinitypoolattwighlight_yndo72.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790173/20Infinitypoolatsunset_vyrchu.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790185/19.Infinitypoolandterraceatdusk_olvlhh.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790150/18.Infinitypoolandterrace_j8gkij.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790147/17.Infinitypoolandsky_gh1x3y.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790165/16.Infinitypoolandrestaurant_sbnbmv.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790145/15.Infinitypoolandrestaurant_naz3d9.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790184/14.Infinitypoolandgreenterrace_niukri.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790175/13.Inifnitypoolandclouds_njelew.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790128/12.Infinitypoolwitheveningskies_vlmbhp.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790172/12.InfinityJacuzziandWhale_sTail_grkiy8.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790174/11InfinityPoolandWhale_sTail_yfxsw1.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790176/11.Infinitypoolandterrace_usyz7l.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790176/4.Breakfast_cfh9lm.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790191/4.7.Aerialviewdistant_krtpm8.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790194/4.6.Aerialviewmidrange_zzesb6.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790194/4.5.Aerialwithoceanview_lqkabz.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790193/2.3.Bar_mdbuvk.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790185/1.DiningRoomandOceanView_uxmkbl.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790157/1.37.ReceptionDesk2_lvrfz4.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790171/1.25.Lounge4_arnbty.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790187/1.24.Lounge3_bu0ugj.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790179/1.23.Lounge2_f8fegh.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790183/1.10.Hotelentrance_yb5bay.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790171/1.9.Diningroomdusk_xv0sjh.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790191/1.8.Aerialcloseup_amxoea.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790177/1.2.Clubhouse_dvgsux.jpg
youtube_url: https://www.facebook.com/211564638926658/videos/2325518951038907/

---
The renovation of the Rancho Pacifico Boutique Hotel, on the hills overlooking the town of Uvita and the whale´s tale bay, perfectly exemplifies what is possible when the visions of the architect and the client align: an aim for a minimal intervention on the site while creating an architectural statement landmark.

A new club house, new pool and lounge area and 2 new suites conform the scope of the works.

The dark palette chosen for the design is the perfect complement to all the greens of the surrounding tropical jungle and the blues of the sky, and enhanced with wooden custom made focal elements, such as the reception teak deck, teak beds and Guanacaste wood countertops in the bathrooms, bathrooms that opens up to a magnificent view of the jungle and the ocean down below.