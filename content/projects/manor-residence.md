---
categories: Residential
title: Manor Residence
featured: true
project_featured_image: https://res.cloudinary.com/dpoitlxyq/image/upload/v1559231919/0_IMG_4521_JPG.jpg
project_gallery:
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559231871/0_905974f8_92ec_4e43_b6d7_508d742ea150_JPG.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559232079/0_IMG_4446_JPG.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559232068/0_IMG_4471_JPG.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559231938/0_IMG_4472_JPG.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559231938/0_IMG_4474_JPG.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559232009/0_IMG_4476_JPG.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559232005/0_IMG_4480_JPG.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559231957/0_IMG_4481_JPG.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559231995/0_IMG_4484_JPG.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559232043/0_IMG_4489_JPG.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559232059/0_IMG_4491_JPG.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559232076/0_IMG_4492_JPG.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559232070/0_IMG_4495_JPG.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559232051/0_IMG_4496_JPG.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559232051/0_IMG_4497_JPG.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559232084/0_IMG_4503_JPG.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559232084/0_IMG_4505_JPG.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559232073/0_IMG_4506_JPG.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559232057/0_IMG_4508_JPG.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559232076/0_IMG_4510_JPG.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559232069/0_IMG_4511_JPG.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559232047/0_IMG_4512_JPG.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559232059/0_IMG_4515_JPG.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559232050/0_IMG_4518_JPG.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559231919/0_IMG_4521_JPG.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559231857/1_residential__manor_residence0_3295d2d9_37ba_4fdc_af16_785c8bdbdc91_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559231856/1_residential__manor_residence0_9700eaee_61db_4b4e_8fb1_e449b789032d_jpg.jpg
youtube_url: https://www.youtube.com/watch?v=Ns8gys2vo04
weight: 85
second_youtube_url: ''
third_youtube_url: ''
fourth_youtube_url: ''
fifth_youtube_url: ''

---
On a narrow building pad with stunning views to the Pacific Ocean and the surrounding tropical jungle, the architecture of this house was designed for maximizing the space on the ground level, achieved thanks to an open floor plan concept that makes room to an amazing infinite edge pool with beaches on both ends for lounging.

For enhancing the sensation of spaciousness on this level, a series of double heights were created, “empty voids “ that let the second floor (where the bedrooms are) having unobstructed views to Dominical beach. Furthermore, these empty voids were not only designed for making the house feel bigger but also for marking the passage of time; the pergolas below the transparent roof on top of them cast different shadows on the walls depending on the time of the day, and this is where, every moment, the house becomes a different sensorial experience to the spectator.