---
categories: Residential
title: Tropical Bungalow -proposal-
featured: false
project_featured_image: https://res.cloudinary.com/dpoitlxyq/image/upload/v1559228183/1_residential__tropical_bungalow2_FRONTAL_noche_jpg.jpg
project_gallery:
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559228178/1_residential__tropical_bungalow1_FRONTAL_temprano_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559228183/1_residential__tropical_bungalow2_FRONTAL_noche_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559228177/1_residential__tropical_bungalow6_desde_el_ban_o_opuesto_abierto_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559228178/1_residential__tropical_bungalow7_desde_el_ban_o_abierto_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559228181/1_residential__tropical_bungalow8_INTERNO_derecho_sala_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559228179/1_residential__tropical_bungalow9_INTERNO_derecha_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559228183/1_residential__tropical_bungalow4_IZQUIERDA_puertas_abiertas_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559228182/1_residential__tropical_bungalow5_IZQUIERDA_noche_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559228180/1_residential__tropical_bungalow5_IZQUIERDA_noche_con_mas_luz_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559228180/1_residential__tropical_bungalow11_DERECHA_noche_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559228181/1_residential__tropical_bungalow10_DERECHA_puertas_abiertas_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559228187/1_residential__tropical_bungalow12_DERECHA_noche_con_mas_luz_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559228182/1_residential__tropical_bungalow3_FRONTAL_noche_con_mas_luz_jpg.jpg
youtube_url: ''
weight: 6
second_youtube_url: ''
third_youtube_url: ''
fourth_youtube_url: ''
fifth_youtube_url: ''

---
In the tropics, life takes place outside; there is no need of thick walls or large glass surfaces for an efficient temperature control, instead tropical architecture is based on spatial continuity, or open floor plans, achieved by using mobile and porous enclosures that let the sunlight and the breeze in, either when the doors are open during the day or when closed during night time.

This strategy is used for the enjoyment of both the surrounding beautiful tropical scenery while enhancing the sensation of freedom of their dwellers.